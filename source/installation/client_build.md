# Build Client from Source

## On Windows
1. Install Visual Studio 2019
    [download Visual Studio](https://visualstudio.microsoft.com/de/)
    * with Xamarin
    * with UWP
    * with .NET Desktop
2. Install GTKSharp for Windows
    [download GTKSharp](https://www.mono-project.com/download/stable/#download-win)
3. Install capnproto

    3.1 If you have Chocolatey installed
    ```shell
    $ choco install capnproto
    ```
    3.2 else you can download it from [here](https://capnproto.org/install.html) and add it to your PATH

4. Clone Borepin
    [download Borepin](https://gitlab.com/fabinfra/fabaccess/client)
6. Load Borepin
7. Build Borepin

If Step 5. Build Borepin is failing because of GTKSharp, it could help to restart your PC.

## Build GTK Project
1. Install mono
    [download mono](https://www.mono-project.com/download/stable/#download-lin)
2. Install mono, gtk-sharp, msbuild, nuget, capnproto
    1.1 Debian based
    ```shell
    $ apt install mono-complete, gtk-sharp2, libcanberra-gtk-module, nuget, capnproto, git
    ```
    1.2 ArchLinux based
    ```shell
    $ pacman -S mono, mono-msbuild, gtk-sharp-2, nuget, capnproto
    ```
3. Update NuGet
    ```shell
    $ nuget update -self
    ```
3. Clone Borepin
    ```shell
    $ git clone https://gitlab.com/fabinfra/fabaccess/client.git --recurse-submodules
    ```
    
4. Build Borepin
    ```shell
    $ cd client
    $ nuget restore
    $ msbuild -t:Borepin_GTK
    ```
4. Run Borepin
    ```shell
    $ mono ./Borepin/Borepin.GTK/bin/Debug/Borepin.GTK.exe
    ```
You can also use Rider or monodevelop as an IDE for development on Borepin

## macOS / iOS

1. Install Visual Studio for Mac 

2. Install capnproto
    If you install capnp with Homebrew you may have to symlink the capnp binary into '/usr/local/bin', or bring it into your PATH another way.

3. Clone Borepin
    ```shell
    $ git clone https://gitlab.com/fabinfra/fabaccess/client.git --recurse-submodules
    ```

4. Open in Visual Studio

5. Build

