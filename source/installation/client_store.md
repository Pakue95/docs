# Get Client from Store

## Hardware Requirements
Minimal Android Version: 5.0 (API Level 21)

Minimal iOS Version: 8.0

## Store Links
+ **Android**: [https://play.google.com/store/apps/details?id=org.fab_infra.fabaccess](https://play.google.com/store/apps/details?id=org.fab_infra.fabaccess)
+ **iOS**: [https://apps.apple.com/us/app/fabaccess/id1531873374](https://apps.apple.com/us/app/fabaccess/id1531873374)

## Beta Links - use on youre own risk
+ **iOS**: [https://testflight.apple.com/join/KfeUaHT4](https://testflight.apple.com/join/KfeUaHT4)